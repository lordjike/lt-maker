# docs/requirements.txt

attrs>=21.4.0
sphinx_rtd_theme==2.0.0
myst-parser==2.0.0
# Needed in order to parse documentation
pygame-ce==2.3.2
typing-extensions==4.8.0